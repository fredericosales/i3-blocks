#!/bin/bash
#     ____ ___  ______   __  __  __    _    ____ _   _ ___ _   _ _____
#    / ___/ _ \|  _ \ \ / / |  \/  |  / \  / ___| | | |_ _| \ | | ____|
#   | |  | | | | |_) \ V /  | |\/| | / _ \| |   | |_| || ||  \| |  _|
#   | |__| |_| |  __/ | |   | |  | |/ ___ \ |___|  _  || || |\  | |___
#    \____\___/|_|    |_|   |_|  |_/_/   \_\____|_| |_|___|_| \_|_____|
#

# var
TLT="copy machine"
MSG="Coping $org to $des"
TIM=1
PER=0
org="${1%/}"
des="${2%/}"
dte=(date +%Y-%m-%d_%H:%M:%S)
dir_des="$des/${org##*/}"

# function
die() {
    echo "Error: $*";
    exit 1;
}

szof() {
    du -s "$1" | cut -f1;
}

doing() {
    ps $1 | grep $1 > /dev/null;
}

# there can be only "TWO"
[ $2 ] || die "Use: $0 origin destiny"

# they must be directories
[ -d "$org" ] || die "The orgin '$org' must be a directory"
[ -d "$des" ] || die "The destiny '$des' must be a directory"

# Dude, are you kidding ? they are the same...
[ "$org" = "$des" ] && [ die "Dude, they are the same..." ]

# the freaking directory is empty?
#dir_des="$des/${org##*/}"
[ -d "$dir_des" ] && [ szof $dir_des -gt 4 ] && [ die "Dude, the destiny ($des) is not empty!" ]

# expand vars
MSG=$(eval echo $MSG)
ALL=$(szof $org)

# coping
cp -r $org $des &
CPPID=$!

# so, I can't play anymore!
trap "kill $CPPID" 2 15

# check loop
while running $CPPID;
do
    cpd=$(szof $dir_des)
    per=$((cpd * 100 / ALL))
    echo $per
    sleep $TIM
done; echo 100 | dialog --title "$TLT" --gauge "$MSG" 8 40 0;
echo "all done"
