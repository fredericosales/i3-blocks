
# Bookmark

* [google](https://google.com)
* [gmail](https://mail.google.com/mail/u/0/#inbox)
* [drive](https://drive.google.com/drive/my-drive)
* [docs](https://docs.google.com/document/u/0/?pli=1&tgif=ds://docs.google.com/document/u/0/?pli=1&tgif=d)
* [calendar](https://calendar.google.com/calendar/r?pli=1)
* [EAD](https://ead.ufjf.br/login/index.php)
* [SIGA](https://siga.ufjf.br/)
* [overleaf](https://www.overleaf.com/project)
* [prime](https://www.amazon.com.br/gp/yourstore/home?ie=UTF8&ref_=nav_custrec_signin&)
* [prime video](https://www.primevideo.com/ref=av_auth_return_redir?_encoding=UTF8&ie=UTF8&ie=UTF8&ref=dvm_crs_prm_br_ra_s_primemember)
* [prime music](https://music.amazon.com.br/home?ref=dm_ws_lnd_pm_listn_pm_77fd7ecb-f330-4d63-804e-cdd587eb6ce5)
* [kindle](https://www.amazon.com.br/kindle-dbs/hz/bookshelf/prime)
* [twitch prime](https://twitch.amazon.com/tp://twitch.amazon.com/tp?)
* [netflix](https://www.netflix.com/br-en/)
* [office](https://www.office.com/?auth=2)
* [youtube](https://www.youtube.com/)
* [shodan](https://www.shodan.io/)
* [thingiverse](https://www.thingiverse.com/)
* [brasil banda larga](https://www.brasilbandalarga.com.br/bbl/)
* [reddit](https://www.reddit.com/)
* [crontab](https://crontab.guru/)
* [diagram](https://www.diagram.codes/)
* [abntex](https://github.com/abntex/biblatex-abnt)
* [gen lib](http://gen.lib.rus.ec/)
* [online gdb](https://www.onlinegdb.com/)
* [tshark](https://tshark.dev/)
* [ezprompt](http://ezprompt.net/)
* [samepage](https://samepage.io/app/#!/1c83e2ce38f96a89525f8da7d2836d645c5398b8/hom://samepage.io/app/#!/1c83e2ce38f96a89525f8da7d2836d645c5398b8/home)
* [regex101](https://regex101.com/)
* [scihub](https://sci-hub.tw/)
* [scihub tw](https://sci-hub.tw/)
* [messages](https://messages.google.com/web/authentication)
* [ascii](https://ascii.co.uk/art)
* [thesaurus](https://www.thesaurus.com/)
* [madmonkey](http://madmonkey-byte.ddns.net/)
* [check host](https://check-host.net/check-udp?lang=en)
* [markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [tablesgenerator](https://www.tablesgenerator.com/)
* [paste debian](http://paste.debian.net/)
* [jupyterlab](http://byte:8888/desperate_cry/lab?)
* [i3-gaps](https://gist.github.com/boreycutts/6417980039760d9d9dac0dd2148d4783)
* [suckless](https://suckless.org/)
* [ranger plugin](https://github.com/LinXueyuanStdio/ranger_icon_plugin)
* [tecmint](https://www.tecmint.com/)
* [bash hackers](http://wiki.bash-hackers.org/)
* [draw - matplotlib](https://www.python-course.eu/matplotlib_legends_and_annotations.php)

# setuptools

#### tree


    foo
    ├── foo
    │   ├── data_struct.py
    │   ├── __init__.py
    │   └── internals.py
    ├── README
    ├── requirements.txt
    ├── scripts
    │   ├── cool
    │   └── skype
    └── setup.py

#### Setup file

* basic (setup.py)

```python
from setuptools import setup

setup(
   name='foo',
   version='1.0',
   description='A useful module',
   author='Man Foo',
   author_email='foomail@foo.com',
   packages=['foo'],  #same as name
   install_requires=['bar', 'greek'], #external packages as dependencies
   scripts=[
            'scripts/cool',
            'scripts/skype',
           ]
)
```

* fancy (setup.py)

```python
from setuptools import setup

with open("README", 'r') as f:
    long_description = f.read()

setup
```

# RE

# RE matches

| RE             	| Meaning                                 	|
|---------------	|-----------------------------------------	|
| .             	| any char                                	|
| *             	| any number of previous (including zero) 	|
| +             	| any number of previous                  	|
| $             	| end of line                             	|
| ^             	| begining of the line                    	|
| \S            	| any non-whitespace char                 	|
| \s            	| any whitespace                          	|
| ?             	| optional char                           	|
| \             	| escape char                             	|
| [a-z]         	| any lowercase char                      	|
| [A-Z]         	| any uppercase char                      	|
| [A-Za-z]      	| any char at all                         	|
| [0-9]         	| any number                              	|
| [0-9][0-9]    	| any number 2 digits                     	|
| [A-Za-z][0-5] 	| any char and any from 0 to 5            	|
 
# Regular Expression Matches

| RE         	| Meaning                                                        	|
|------------	|----------------------------------------------------------------	|
| *          	| Any line with an asterisk                                      	|
| \\*         	| Any line with an asterisk                                      	|
| \\         	| Any line with a backslash                                      	|
| ^*         	| Any line starting with an asterisk                             	|
| ^A*        	| Any line                                                       	|
| ^A\*       	| Any line starting with an "A*"                                 	|
| ^AA*       	| Any line if it starts with one "A"                             	|
| ^AA*B      	| Any line with one or more "A"'s followed by a "B"              	|
| ^A\{4,8\}B 	| Any line starting with 4, 5, 6, 7 or 8 "A"'s followed by a "B" 	|
| ^A\{4,\}B  	| Any line starting with 4 or more "A"'s followed by a "B"       	|
| \{4,8\}    	| # Any line with "{4,8}"                                        	|
| A{4,8}     	| # Any line with "A{4,8}"                                       	|

# Regular Expression Matches

| RE           	| Class      	| Type          	| Meaning                                            	|
|--------------	|------------	|---------------	|----------------------------------------------------	|
| .            	| all        	| Character Set 	| Single character (except newline)                  	|
| ^            	| all        	| Anchor        	| Beginning of line                                  	|
| $            	| all        	| Anchor        	| End of line                                        	|
| [...]        	| all        	| Character Set 	| Range of characters                                	|
| *a\<         	| Basic      	| Anchor        	| Beginning of word                                  	|
| \>           	| Basic      	| Anchor        	| End of word                                        	|
| \(..\)       	| Basic      	| Backreference 	| Remembers pattern                                  	|
| \1..\9       	| Basic      	| Reference     	| Recalls pattern                                    	|
| _+           	| Extended   	| Modifier      	| One or more duplicates                             	|
| ?            	| Extended   	| Modifier      	| Zero or one duplicate                              	|
| \{MN\}       	| Extended   	| Modifier      	| M to N Duplicates                                  	|
| (...\|...)   	| Extended   	| Anchor        	| Shows alteration                                   	|
| \(...\|...\) 	| alteration 	| EMACS         	| Anchor shows alteration                            	|
| \w           	| EMACS      	| Character set 	| Matches a letter in a word                         	|
| \W           	| EMACS      	| Character set 	| Opposite of \wll Modifier zero or more duplicates* 	|
 
# e-mail RE

```sh
[user@machine]$:grep "\S+@\S\+\.[A-Za-z]\+"
```

# linux audio stuff
    
    mpc 
    mpc toggle
    mpc pause
    mpc next
    mpc prev
    mpc repeat
    mpc status  (general info and stuff like that)
    mpc current (music info)
    
    amixer (ALSA)
    amixer sset Master toggle
    amixer sset Master mute
    amixer sset Master 5%-
    amixer sset Master 5%+
    
    pulsemixer (PULSE)
    pulsemixer --toggle-mute
    pulsemixer --mute
    pulsemixer --change-volume +5
    pulsemixer --change-volume -5

# weather
```sh
[user@machine]$: curl wttr.in
```

# Bash [cheatsheet] 

# Brace expansion

| CMD           	| Exit      	|
|---------------	|-----------	|
| echo {AB}.js  	| AB.js     	|
| echo {A,B}    	| A B       	|
| echo {A,B}.js 	| A.js B.js 	|
| echo {1..5}   	| 1 2 3 4 5 	|

# Parameters expansions
```sh
[user@machine]$: name="John"
```
| CMD                   | Exit                          |
|---------------------	|-----------------------------	|
| echo ${name}        	| => "John"                   	|
| echo ${name/J/j}    	| => "john" (substitution)    	|
| echo ${name:0:2}    	| => "Jo" (slicing)           	|
| echo ${name::2}     	| => "Jo" (slicing)           	|
| echo ${name::-1}    	| => "Joh" (slicing)          	|
| echo ${name:(-1)}   	| => "n" (slicing from right) 	|
| echo ${name:(-2):1} 	| => "h" (slicing from right) 	|
| echo ${food:-Cake}  	| => $food or "Cake"          	|

# length=2
```sh
[user@machine]$: name="John"
[user@machine]$: echo ${name:0:length}  
[user@machine]$: Jo
```
# STR

```sh
STR="/path/to/foo.cpp"
```

| CMD                 	| Exit               	|
|---------------------  |--------------------	|
| echo ${STR%.cpp}    	| /path/to/foo       	|
| echo ${STR%.cpp}.o  	| /path/to/foo.o     	|
| echo ${STR%/*}      	| /path/to           	|
| echo ${STR##*.}     	| cpp (extension)    	|
| echo ${STR##*/}     	| foo.cpp (basepath) 	|
| echo ${STR#*/}      	| path/to/foo.cpp    	|
| echo ${STR##*/}     	| foo.cpp            	|
| echo ${STR/foo/bar} 	| /path/to/bar.cpp   	|

# STR
```sh
[user@machine]$: STR="Hello world"
```

| CMD               | Exit      |
|----------------   |---------  |
|echo ${STR:6:5}    | "world"   |
|echo ${STR:-5:5}   | "world"   |

# SRC
```sh
[user@machine]$: SRC="/path/to/foo.cpp"
```

| CMD               | Exit                      |
|-----------------  |-------------------------  |
| BASE=${SRC##*/}   | => "foo.cpp" (basepath)   |
| DIR=${SRC%$BASE}  | => "/path/to/" (dirpath)  |

# Substitution

| CMD               | Exit                      |
|-----------------  |-------------------------  |
| ${FOO%suffix}	    | Remove suffix             |
| ${FOO\|prefix}    | Remove prefix             |
| ${FOO%%suffix}	| Remove long suffix        |
| ${FOO\|\|prefix}	| Remove long prefix        |
| ${FOO/from/to}	| Replace first match       |
| ${FOO//from/to}	| Replace all               |
| ${FOO/%from/to}	| Replace suffix            |
| ${FOO/\|from/to}	| Replace prefix            |

# Lenght
```sh
[user@machine]$: ${#FOO} # Length of $FOO
```

# Default values


| CMD               | Exit                                                      |
|-----------------  |---------------------------------------------------------  |
| ${FOO:-val}	    | $FOO, or val if unset (or null)                           |
| ${FOO:=val}     	| Set $FOO to val if unset (or null)                        |
| ${FOO:+val}       | val if $FOO is set (and not null)                         |
| ${FOO:?message}   | Show error message and exit if $FOO is unset (or null)    |

# Omitting the : removes the (non)nullity checks, 

```sh
[user@machine]$: ${#FOO}    # Length of $FOO
[user@machine]$: ${FOO-val} # expands to val if unset otherwise $FOO.
```
# Comments

```sh
# Single line comment

# Multilines
: '
This is a
multi line
comment
'
```
# Substrings
| CMD           | Exit                          |
|-----------    |---------------------------    |
|${FOO:0:3}		| Substring (position, length)  |
|${FOO:(-3):3}  | Substring from the right      |

# Manipulation

```sh
[user@machine]$: STR="HELLO WORLD!"
[user@machine]$: echo ${STR,}    #=> "hELLO WORLD!" (lowercase 1st letter)
[user@machine]$: echo ${STR,,}   #=> "hello world!" (all lowercase)
```

```sh
STR="hello world!"
echo ${STR^}    #=> "Hello world!" (uppercase 1st letter)
echo ${STR^^}   #=> "HELLO WORLD!" (all uppercase)
```

# Loops

```sh
for i in /etc/rc.*; do
	echo $i
done

for ((i = 0 ; i < 100 ; i++)); do
	echo $i
done

for i in {1..5}; do
	echo "Welcome $i"
done

for i in {5..50..5}; do
	echo "Welcome $i"
done
```
# Reading lines

```sh
cat file.txt | while read line; do
	echo $line
done

while true; do
	···
done
```

# Functions

```sh
myfunc() {
	echo "hello $1"
}

# Same as above (alternate syntax)
function myfunc() {
	echo "hello $1"
}

myfunc "John"

# Returning values
myfunc() {
	local myresult='some value'
    echo $myresult
}

result="$(myfunc)"
```

# Raising errors

```sh
myfunc() {
	return 1
}

if myfunc; then
	echo "success"
else
	echo "failure"
fi
```

# Arguments

|Simbol | Meaning                               |
|-----  |-----------------------------------    |
|$#     | Number of arguments                   |
|$*     | All arguments                         |
|$@ 	| All arguments, starting from first    |
|$1 	| First argument                        |
|$_ 	| Last argument of the previous command |

# conditionals

| CMD                       | Meaning                     |
|-----------------------    |-------------------------    |
| [[ -z STRING ]]           | Empty string                |
| [[ -n STRING ]]	        | Not empty string            |
| [[ STRING == STRING ]]    | Equal                       |
| [[ STRING != STRING ]]    | Not Equal                   |
| [[ NUM -eq NUM ]]         | Equal                       |
| [[ NUM -ne NUM ]]         | Not equal                   |
| [[ NUM -lt NUM ]]         | Less than                   |
| [[ NUM -le NUM ]]         | Less than or equal          |
| [[ NUM -gt NUM ]]         | Greater than                |
| [[ NUM -ge NUM ]]         | Greater than or equal       |
| [[ STRING =~ STRING ]]    | Regexp                      |
| (( NUM < NUM ))	        | Numeric conditions          |
| [[ -o noclobber ]]	    | If OPTIONNAME is enabled    |
| [[ ! EXPR ]]	            | Not                         |
| [[ X && Y ]]	            | And                         |
| [[ X \|\| Y ]]	        | Or                          |

# File conditions

| CMD                   | Meaning                   |
|-------------------    |-----------------------    |
| [[ -e FILE ]]  		| Exists                    |
| [[ -r FILE ]]  		| Readable                  |
| [[ -h FILE ]]  		| Symlink                   |
| [[ -d FILE ]]  		| Directory                 |
| [[ -w FILE ]]  		| Writable                  |
| [[ -s FILE ]]  		| Size is > 0 bytes         |
| [[ -f FILE ]]  		| File                      |
| [[ -x FILE ]]  		| Executable                |
| [[ FILE1 -nt FILE2 ]] | 1 is more recent than 2   |
| [[ FILE1 -ot FILE2 ]] | 2 is more recent than 1   |
| [[ FILE1 -ef FILE2 ]] | Same files                |

# Example
# String

```sh
if [[ -z "$string" ]]; then
	echo "String is empty"
elif [[ -n "$string" ]]; then
	echo "String is not empty"
fi
```

# Combinations

```sh
if [[ X && Y ]]; then
	...
fi
```
# Equal

```sh 
if [[ "$A" == "$B" ]]
```

# Regex

```sh 
if [[ "A" =~ . ]]

if (( $a < $b )); then
	echo "$a is smaller than $b"
fi
         
if [[ -e "file.txt" ]]; then
	echo "file exists"
fi
```
# Arrays
```sh
Fruits=('Apple' 'Banana' 'Orange')
Fruits[0]="Apple"
Fruits[1]="Banana"
Fruits[2]="Orange"
```

# Operations

```sh
Fruits=("${Fruits[@]}" "Watermelon")    # Push                 
Fruits+=('Watermelon')                  # Also Push            
Fruits=( ${Fruits[@]/Ap*/} )            # Remove by regex match
unset Fruits[2]                         # Remove one item      
Fruits=("${Fruits[@]}")                 # Duplicate            
Fruits=("${Fruits[@]}" "${Veggies[@]}") # Concatenate          
lines=(`cat "logfile"`)                 # Read from file       
```

# Working with arrays

```sh
echo ${Fruits[0]}       # Element #0                       
echo ${Fruits[-1]}      # Last element                     
echo ${Fruits[@]}       # All elements, space-separated    
echo ${#Fruits[@]}      # Number of elements               
echo ${#Fruits}         # String length of the 1st element 
echo ${#Fruits[3]}      # String length of the Nth element 
echo ${Fruits[@]:3:2}   # Range (from position 3, length 2)
```

# Iterations

```sh
for i in "${arrayName[@]}"; do
	echo $i
done
```

# Dictionaries

```sh
declare -A sounds
sounds[dog]="bark"
sounds[cow]="moo"
sounds[bird]="tweet"
sounds[wolf]="howl"
```

# Working with dictionaries

```sh
echo ${sounds[dog]}     # Dog's sound
echo ${sounds[@]}       # All values
echo ${!sounds[@]}      # All keys
echo ${#sounds[@]}      # Number of elements
unset sounds[dog]       # Delete dog
```

# Iteration

```sh
for val in "${sounds[@]}"; do
	echo $val
done

for key in "${!sounds[@]}"; do
	echo $key
done
```

# Option

```sh
set -o noclobber    # Avoid overlay files (echo "hi" > foo)
set -o errexit      # Used to exit upon error, avoiding cascading errors
set -o pipefail     # Unveils hidden failures
set -o nounset      # Exposes unset variables
```

# Glob options

```sh
shopt -s nullglob   		   # Non-matching globs are removed  ('*.foo' => '')
shopt -s failglob   		   # Non-matching globs throw errors
shopt -s nocaseglob 		   # Case insensitive globs
shopt -s dotglob    		   # Wildcards match dotfiles ("*.sh" => ".foo.sh")
shopt -s globstar   		   # Allow ** for recursive matches ('lib/**/*.rb' => 'lib/a/b/c.rb')
```

# History

```sh
history Show history
shopt -s histverify Don’t execute expanded result immediately
```

# Operations

| CMD                   | Meaning                                                           |
|-------------------    |---------------------------------------------------------------    |           
| !!  					| Execute last command again                                        |
| !!:s/<FROM>/<TO>/   	| Replace first occurrence of <FROM> to <TO> in most recent command |
| !!:gs/<FROM>/<TO>/  	| Replace all occurrences of <FROM> to <TO> in most recent command  |
| !$:t    				| Expand only basename from last parameter of most recent command   |
| !$:h    				| Expand only directory from last parameter of most recent command  |
| !! and !$ 			| can be replaced with any valid expansion.                         |

# Expansions

| CMD           | Meaning                                               |
|-----------    |---------------------------------------------------    |           
| !$ 			| Expand last parameter of most recent command          |
| !* 			| Expand all parameters of most recent command          |
| !-n			| Expand nth most recent command                        |
| !n 			| Expand nth command in history                         |
| !<command>    | Expand most recent invocation of command <command>    |

# Slices

| CMD       | Meaning                                                                               |
|-------    |-----------------------------------------------------------------------------------    |           
| !!:n   	| Expand only nth token from most recent command (command is 0; first argument is 1)    |
| !^     	| Expand first argument from most recent command                                        |
| !$ 	   	| Expand last token from most recent command                                            |
| !!:n-m    | Expand range of tokens from most recent command                                       |
| !!:n-$ 	| Expand nth token to last from most recent command                                     |
| !! 	   	| can be replaced with any valid expansion i.e. !cat, !-2, !42, etc.                    |

# Miscellaneous

```sh
$((a + 200))      		# Add 200 to $a
$((RANDOM%=200))  		# Random number 0..200
```

# Inspecting commands

```sh
command -V cd			#=> "cd is a function/alias/whatever"
```

# Trap errors

```sh
trap 'echo Error at about $LINENO' ERR

# or

traperr() {
	echo "ERROR: ${BASH_SOURCE[1]} at about ${BASH_LINENO[0]}"
}

set -o errtrace
trap traperr ERR
```

# Subshells

```sh
(cd somedir; echo "I'm now in $PWD")
pwd 					# still in first directory
```

# Redirection

| python hello.py > output.txt  	| stdout to (file)                 	|
|-------------------------------	|----------------------------------	|
| python hello.py >> output.txt 	| stdout to (file) - append        	|
| python hello.py 2> error.log  	| stderr to (file)                 	|
| python hello.py 2>&1          	| stderr to stdout                 	|
| python hello.py 2>/dev/null   	| stderr to (null)                 	|
| python hello.py &>/dev/null   	| stdout and stderr to (null)      	|
| python hello.py < foo.txt     	| feed foo.txt to stdin for python 	|

# Case/switch

```sh
case "$1" in
	start | up)
		vagrant up ;;

	*)
		echo "Usage: $0 {start|stop|ssh}" ;;
esac
```

# Source relative

```sh
source "${0%/*}/../share/foo.sh"
```

# Directory of script

```sh
DIR="${0%/*}"
```

# Getting options

```sh
while [[ "$1" =~ ^- && ! "$1" == "--" ]]; do case $1 in
    -V | --version )
    	echo $version
        exit ;;
     
    -s | --string )
    	shift; string=$1 ;;
                                  
    -f | --flag )
    	flag=1 ;;
    
    esac; shift; done

if [[ "$1" == '--' ]]; then shift; fi
```

# Special Variables 

| Simbol 	| Meaning                      	|
|--------	|------------------------------	|
| $?     	| Exit status of task          	|
| $!     	| PID of last background task  	|
| $$     	| PID of shell                 	|
| $0     	| Filename of the shell script 	|

# Check for command’s result

```sh
if ping -c 1 google.com; then
	echo "It appears you have a working internet connection"
fi
```

# printf

```sh
printf "Hello %s, I'm %s" Sven Olga				#=> "Hello Sven, I'm Olga
printf "1 + 1 = %d" 2							#=> "1 + 1 = 2"
printf "This is how you print a float: %f" 2	#=> "This is how you print a float: 2.000000"
```

# Heredoc

```sh
cat <<END
hello world
END
```

# Reading input

```sh
echo -n "Proceed? [y/n]: "
read ans
echo $ans
read -n 1 ans    			# Just one character
```

# Go to previous directory

```sh
pwd 						# /home/user/foo
cd bar/
pwd 						# /home/user/foo/bar
cd -
pwd 						# /home/user/foo
```
	
# Grep check
```sh
if grep -q 'foo' ~/.bash_history; then
	echo "You appear to have typed 'foo' in the past"
fi
```

# Config files

```sh
#!/bin/bash
echo "Reading config...." >&2
source /etc/cool.cfg
echo "Config for the username: $cool_username" >&2
echo "Config for the target host: $cool_host" >&2

# config.cfg
cool_username="guest"
cool_host="foo.example.com"
```

# Add colors to your scripts

```sh
# Colors
ESC_SEQ="\x1b["
COL_RESET=$ESC_SEQ"39;49;00m"
COL_RED=$ESC_SEQ"31;01m"
COL_GREEN=$ESC_SEQ"32;01m"
COL_YELLOW=$ESC_SEQ"33;01m"
COL_BLUE=$ESC_SEQ"34;01m"
COL_MAGENTA=$ESC_SEQ"35;01m"
COL_CYAN=$ESC_SEQ"36;01m"

# Use
echo -e "$COL_RED This is red $COL_RESET"
echo -e "$COL_BLUE This is blue $COL_RESET"
echo -e "$COL_YELLOW This is yellow $COL_RESET"
```
  
