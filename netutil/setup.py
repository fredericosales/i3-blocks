from setuptools import setup, find_packages

setup(
        name='netutil',
        version='1.0',
        install_requires=[
            'Click',
        ],
        entry_points="""
        [console_scripts]
        netutil=netutil:net
        """,
)
