#!/bin/bash
#
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br>
#    _   _ _____    _    ____ _____       _   _    ____ _  __
#   | | | | ____|  / \  |  _ \___  |     | | / \  / ___| |/ /
#   | |_| |  _|   / _ \ | | | | / /   _  | |/ _ \| |   | ' /
#   |  _  | |___ / ___ \| |_| |/ /   | |_| / ___ \ |___| . \
#   |_| |_|_____/_/   \_\____//_/     \___/_/   \_\____|_|\_\
#   
#
case $3 in 
    plug)
        amixer sset Master 10%
        logger "Headphones plugged"
        su frederico -c 'notify-send "Headphones:" "plugged"'
        ;;
        

    unplug)
        amixer sset Master toggle
        logger "Headphones unplugged"
        mpc pause
        su frederico -c 'notify-send "Headphones:" "unplugged"'
        ;;
esac
